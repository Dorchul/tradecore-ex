from django.conf import settings
from django.core import validators
from pyhunter import PyHunter
from rest_framework import serializers

from accounts import models


def hunter_email_validator(value):
    if not settings.USE_HUNTER:
        return
    hunter = PyHunter(settings.HUNTER_API_KEY)
    email_verification = hunter.email_verifier(value)
    if email_verification['status'] == 'invalid':
        raise serializers.ValidationError('Invalid email address')


class UserEnrichmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.UserEnrichment
        fields = '__all__'


class RegisterUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'},
                                     write_only=True)
    email = serializers.EmailField(
        required=True,
        validators=[validators.EmailValidator, hunter_email_validator])
    enriched = UserEnrichmentSerializer(read_only=True)

    class Meta:
        model = models.User
        fields = ('id', 'username', 'email', 'password', 'enriched')

    def create(self, validated_data):
        # NOTE: create_user must be called to set save the password hash
        # correctly, otherwise set_password needs to be used.
        return models.User.objects.create_user(**validated_data)


class LikeSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField(source='user.id')
    username = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = models.Like
        fields = ('id', 'user_id', 'username', 'post', 'date')

    def create(self, validated_data):
        return models.Like.objects.create(**validated_data)


class PostSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    likes = LikeSerializer(many=True, read_only=True)
    author_id = serializers.ReadOnlyField(source='author.id')
    author_username = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = models.Post
        fields = ('id', 'author_id', 'author_username', 'creation_date',
                  'content', 'likes')

    def create(self, validated_data):
        return models.Post.objects.create(**validated_data)
