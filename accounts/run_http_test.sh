#!/usr/bin/env bash

# See https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -o errexit -o errtrace -o nounset -o pipefail

readonly DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
readonly URI_PREFIX='http://127.0.0.1:8000/api/v1'

_get_auth_header() {
  local user="$1"
  local token
  token="$(curl --silent -X POST -H "Content-Type: application/json" -d \
    '{"username": "'"${user}"'", "password": "'"${user}"'"}' \
    http://localhost:8000/api/v1/token | jq --raw-output '.access')"
  printf 'Authorization:Bearer %s' "${token}"
}

_reset_db() {
  \rm -rf db.sqlite3 accounts/migrations/00*py &> /dev/null || true
  python manage.py makemigrations
  python manage.py migrate
  sleep 1
}

main() {
  cd "${DIR}/.."
  _reset_db

  # Create users a, b, c
  http POST "${URI_PREFIX}/users" 'username=a' 'password=a' 'email=a@mail.com'
  http POST "${URI_PREFIX}/users" 'username=b' 'password=b' 'email=b@mail.com'
  http POST "${URI_PREFIX}/users" 'username=c' 'password=c' 'email=c@mail.com'

  # Create posts
  auth="$(_get_auth_header a)"
  http POST "${URI_PREFIX}/posts" "${auth}" content='post a'
  auth="$(_get_auth_header b)"
  http POST "${URI_PREFIX}/posts" "${auth}" content='post b'
  auth="$(_get_auth_header c)"
  http POST "${URI_PREFIX}/posts" "${auth}" content='post c'

  # Like posts
  auth="$(_get_auth_header a)"
  http POST "${URI_PREFIX}/likes" "${auth}" post=2
  auth="$(_get_auth_header b)"
  http POST "${URI_PREFIX}/likes" "${auth}" post=3
  auth="$(_get_auth_header c)"
  http POST "${URI_PREFIX}/likes" "${auth}" post=2

  # List posts
  http GET "${URI_PREFIX}/posts" "${auth}"
}

main
