from django.contrib import admin
from django.urls import include
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework_simplejwt.views import TokenVerifyView

from accounts import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # Registation and auth
    path('api/v1/token', TokenObtainPairView.as_view()),
    path('api/v1/token/refresh', TokenRefreshView.as_view()),
    path('api/v1/token/verify', TokenVerifyView.as_view()),
    path('api/v1/auth/', include('rest_framework.urls')),
    # Data/API objects
    path('api/v1/users', views.register_user),
    path('api/v1/posts', views.PostList.as_view()),
    path('api/v1/likes', views.LikeList.as_view()),
    path('api/v1/likes/<int:pk>', views.LikeDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
