import argparse
import collections
import configparser
from dataclasses import dataclass
import random
import string
import sys

import requests
from requests.api import post

CONFIG_FIELD_TO_TYPE = {
    'num_users': int,
    'max_posts_per_user': int,
    'max_likes_per_user': int
}


@dataclass
class BotConfig:
    num_users: int
    max_posts_per_user: int
    max_likes_per_user: int


def get_random_username(length):
    char_set = string.ascii_letters + string.digits
    return ''.join(random.choice(char_set) for i in range(length))


def get_random_password(length):
    char_set = string.printable
    return ''.join(random.choice(char_set) for i in range(length))


# pylint: disable=too-many-instance-attributes
class Bot:

    def __init__(self, server_addr: str, config: BotConfig):
        self.server_addr = server_addr
        self.config = config

        self.user_to_password = {}
        self.user_to_tokens = {}

        self.user_to_posts = collections.defaultdict(set)
        self.user_to_posts_count = collections.Counter()
        self.user_to_liked_posts = collections.defaultdict(set)

        self.post_to_user = {}
        self.posts_with_zero_likes = set()

    def _get_url(self, path: str):
        assert not path.startswith('/')
        return f'{self.server_addr}/api/v1/{path}'

    def _register_user(self, user_index):
        username = f'user{user_index}'
        # Use the username as the password for simplicity.
        self.user_to_password[username] = username
        request_data = {
            'username': username,
            'password': self.user_to_password[username],
            'email': f'{username}@gmail.com',
        }
        response = requests.post(self._get_url('users'), data=request_data)
        response.raise_for_status()
        response = response.json()
        self.user_to_tokens[response['username']] = self._request_new_tokens(
            username)
        return response

    def _request_new_tokens(self, username):
        request_data = {
            'username': username,
            'password': self.user_to_password[username]
        }
        return requests.post(self._get_url('token'), data=request_data).json()

    def create_user_posts(self, username, num_posts):
        token = self.user_to_tokens[username]['access']
        headers = {'Authorization': f'Bearer {token}'}

        for _ in range(num_posts):
            request_data = {'content': 'this is my post'}
            response = requests.post(self._get_url('posts'),
                                     data=request_data,
                                     headers=headers).json()
            post_id = response['id']
            self.user_to_posts[username].add(post_id)
            self.user_to_posts_count[username] += 1
            self.posts_with_zero_likes.add(post_id)
            self.post_to_user[post_id] = username

    def create_user_likes(self, username):
        token = self.user_to_tokens[username]['access']
        headers = {'Authorization': f'Bearer {token}'}
        count = 0
        relevant_posts = self.get_relevant_posts(username)
        print(relevant_posts)

        while relevant_posts and count < self.config.max_likes_per_user:
            post_id = random.choice(list(relevant_posts))
            request_data = {'post': post_id}
            response = requests.post(self._get_url('likes'),
                                     data=request_data,
                                     headers=headers)
            count += 1
            relevant_posts.remove(post_id)
            if post_id in self.posts_with_zero_likes:
                user = self.post_to_user[post_id]
                self.posts_with_zero_likes.remove(post_id)
                for post in self.user_to_posts[user]:
                    if post in relevant_posts:
                        relevant_posts.remove(post)

    def get_relevant_posts(self, username):
        relevant_users = set(
            self.post_to_user[post] for post in self.posts_with_zero_likes)
        relevant_posts = set()
        for user in relevant_users:
            if user == username:
                continue
            for post in self.user_to_posts[user]:
                if post not in self.user_to_liked_posts:
                    relevant_posts.add(post)
        return relevant_posts

    def run(self):

        for i in range(self.config.num_users):
            user = self._register_user(i)
            self.create_user_posts(
                user['username'],
                random.randint(1, self.config.max_posts_per_user))
        print('finished signup and posts')
        for user in self.user_to_posts_count.most_common():

            if self.posts_with_zero_likes:
                self.create_user_likes(user[0])

            else:
                print('no more posts with 0 likes')
                break
        print('finished likes')


def _parse_bot_config(config_file_path: str) -> BotConfig:
    config_parser = configparser.ConfigParser()
    with open(config_file_path) as f:
        content = f.read()
    config_parser.read_string(content)
    if 'bot' not in config_parser.sections():
        raise ValueError('Must have a [bot] section')
    section = config_parser['bot']
    fields = {}
    for field_name, field_type in CONFIG_FIELD_TO_TYPE.items():
        field_value = section.get(field_name)
        if field_value is None:
            raise ValueError(f'Missing required field: {field_name}')
        field_value = field_type(section[field_name])
        fields[field_name] = field_value
    return BotConfig(**fields)


def main():
    arg_parser = argparse.ArgumentParser(
        description='Bot to test TradeCore ex API')
    arg_parser.add_argument('--server-address', default='http://localhost:8000')
    arg_parser.add_argument('--config-file', required=True)
    arg_parser.add_argument('--random-seed', type=int, default=0)
    args = arg_parser.parse_args()
    try:
        config = _parse_bot_config(args.config_file)
    except ValueError as e:
        sys.exit(f'Error parsing config: {e}')
    print(f'Running bot with config: {config}')
    random.seed(args.random_seed)
    bot = Bot(args.server_address, config)
    bot.run()


if __name__ == '__main__':
    main()
