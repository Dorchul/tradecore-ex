import os.path

import setuptools

README_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                           'README.md')
with open(README_PATH) as f:
    LONG_DESCRIPTION = f.read()

setuptools.setup(
    name='avatrade-ex',
    version='0.0.1',
    description='exercise for Avatrade',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    python_requires='>=3.7',
    author='dorchul',
    author_email='dorsev1189@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    packages=setuptools.find_packages(exclude=['tests']),
    package_dir={'': '.'},
    package_data={},
    install_requires=[
        'django>=3.1,<4',
        'djangorestframework>=3.12,<4',
        'djangorestframework-simplejwt>=4.4.0,<5',
        'requests>=2.2,<3',
        'pyhunter>=1.7,<4',
        'clearbit>=0.1.7,<0.2',
    ],
    extras_require={
        'dev': [
            'coverage==4.*,>=4.4.2',
            'isort==5.*,>=5.6.0',
            'jedi==0.*,>=0.17.2',
            'pycodestyle==2.*,>=2.4.0',
            'pydocstyle==3.*,>=3.0.0',
            'pylint==2.*,>=2.6.0',
            'pylint-django==2.*,>=2.3',
            'pytest==4.*,>=4.0.0',
            'pytest-cov==2.*,>=2.6.0',
            'pytype==2020.*,>=2020.10.8',
            'tox==3.*,>=3.5.0',
            'httpie==2.*,>=2.1.0',
        ],
    },
    scripts=[
        'bot/bot.py',
    ],
)
